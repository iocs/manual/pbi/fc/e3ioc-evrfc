# e3ioc-evrfc

e3 ioc - EVR Faraday Cups

## Cloning

Clone this IOC with `git clone https://gitlab.esss.lu.se/iocs/pbi/fc/e3ioc-evrfc.git`.

## Running the IOC

This IOC doesn't need to be compiled nor installed in a specific directory. Just make sure that the versions of the modules required in `st.cmd` are installed for the EPICS base and `require` version that you are using.

If your E3 environment is activated, just go to the top directory of this IOC and run `iocsh.bash st.cmd`.

## History

Originally this IOC was manually written by Jerzy locally in pbi-fc01-mtca-ioc.tn.esss.lu.se under /nfs/ts/nbs/Ctrl-EVR-101.

